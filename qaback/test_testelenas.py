import os
import sys
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

class TestTestelenas():
  def setup_method(self, method):
    os.environ['MOZ_HEADLESS'] = '1'
    self.binary = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
    self.driver = webdriver.Firefox(firefox_binary=self.binary, executable_path=r'C:\\geckodriver.exe')
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_testelenas(self):
    wait = WebDriverWait(self.driver, 90)
    self.driver.get("http://127.0.0.1:8080/polls/1/")
    time.sleep(2)
    self.driver.set_window_size(550, 695) 
    self.driver.find_element(By.CSS_SELECTOR, "label:nth-child(3)").click()
    self.driver.find_element(By.CSS_SELECTOR, "input:nth-child(11)").click()
    self.driver.find_element(By.LINK_TEXT, "Vote again?").click()
    self.driver.find_element(By.CSS_SELECTOR, "label:nth-child(9)").click()
    self.driver.find_element(By.CSS_SELECTOR, "input:nth-child(11)").click()
    self.driver.find_element(By.LINK_TEXT, "Vote again?").click()
    self.driver.find_element(By.CSS_SELECTOR, "label:nth-child(6)").click()
    self.driver.find_element(By.CSS_SELECTOR, "input:nth-child(11)").click()
  
